FROM ubuntu:16.04 
MAINTAINER FFG 

RUN apt-get update -q
RUN apt-get upgrade -qy
RUN apt-get install -qy \
            libimage-exiftool-perl \
            antiword libav-tools \
            imagemagick ghostscript \
            xpdf \
            cron \
            wget \
            apache2 \
            php7.0 \
            php7.0-dev \
            php7.0-gd \
            php7.0-mysql \
            php-mbstring \
            php-zip \
            php-mysql \
            mysql-client \
            libapache2-mod-php \
            subversion

RUN echo "Working" > /var/www/html/index.html

RUN rm -rf /var/www/html && mkdir -p /var/www/html
WORKDIR /var/www/html
RUN svn co http://svn.resourcespace.com/svn/rs/releases/8.1 .
RUN mkdir filestore && chmod 777 filestore && chmod -R 777 include
CMD ["apache2ctl","-D", "FOREGROUND"]

EXPOSE 80
